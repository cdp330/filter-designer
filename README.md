
Python workbook for converting a continuous time transfer function into an IIR filter.
The filter is then converted to a line of C code.


### Example output:
![usage](sample.png)