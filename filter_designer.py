# %%
import scipy.signal as signal
import numpy as np
import matplotlib.pyplot as plt
from IPython.display import display, Math, Markdown
from decimal import Decimal
import warnings
import sympy as sp
import lcapy as lc
from lcapy import s
warnings.filterwarnings("ignore", category=UserWarning)


# %%
def display_equal(name: str, expr):
    display(Math(name + ' ' + sp.latex(expr)))

H_continuous = (s+1e3)/((s+50)*(s+20))
display(Markdown("### Continuous time transfer function"))
display_equal("H_{cont} = ", H_continuous)
num_cont, denom_cont = [i.coeffs().fval for i in [H_continuous.N, H_continuous.D]]

RANGE = np.logspace(-2, 7, 1000)
w_cont, h_cont = signal.freqs(num_cont, denom_cont, worN=RANGE)
w_cont /= np.pi * 2


def bode(w, h, title):
    fig, ax = plt.subplots(2)
    for axes in ax:
        axes.grid(True, which="both")
    ax[0].set_title(title)
    ax[0].semilogx(w, 20*np.log10(np.abs(h)), color="blue")
    ax[1].semilogx(w, np.rad2deg(np.unwrap(np.angle(h))), color="red")

bode(w_cont, h_cont, "Continuous time bode plot")



# %%
display(Markdown("### Discrete time design"))

ws = 1e6 * 2*np.pi   
Ts = 2*np.pi / ws

display(Math(f"\omega_s = {ws} \\text{{ rad/s}}"))
display(Math(f"T_s = {Ts} \\text{{ s }}"))

# Use scipy for the bilinear transform. I found that lcapy calculates correct coefficients but has the wrong amplitude. 
num, denom = signal.bilinear(num_cont, denom_cont, ws / (2*np.pi))
w, h = signal.freqz(num, denom, fs=ws, worN=RANGE)
w /= np.pi*2


fig, ax = plt.subplots(2)
for axes in ax:
    axes.grid(True, which="both")
ax[0].set_title("Discrete vs analog bode plot")
ax[0].semilogx(w, 20*np.log10(np.abs(h)), color="blue", label="Discrete")
ax[0].semilogx(w_cont, 20*np.log10(np.abs(h_cont)), color="green", label="Continuous")

ax[1].semilogx(w, np.rad2deg(np.unwrap(np.angle(h))), color="red", label="Discrete")
ax[1].semilogx(w_cont, np.rad2deg(np.unwrap(np.angle(h_cont))), color="orange", label="Continuous")
fig.legend()


# %%
import re
display(Markdown("### Difference equation"))

# diff = H_z.difference_equation()
diff = lc.DLTIFilter(num, denom).difference_equation()

# display_equal('', H_z)
# display(Math("\\Big\downarrow"))
display_equal('', diff)
lines = []
for key, value in diff.rhs.as_coefficients_dict().items():
    key = str(key)
    index = re.findall(r"\d+", key)
    if len(index) == 0:
        varname = key[0] 
    else:
        varname = key[0] + index[0]


    lines.append(f"({float(value):.8f} * {varname})")

line = "y = "
line += '+'.join(lines)
line += ";"
display(Markdown("### Code output"))
display(Markdown("`" + line + "`"))






# %%



